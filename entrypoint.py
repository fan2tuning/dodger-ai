import os


if __name__ == "__main__":
    ENTRYPOINT = os.getenv("ENTRYPOINT")
    if ENTRYPOINT:
        os.system(f"python3 {ENTRYPOINT}")
    else:
        print("No entrypoint script defined. Please add a script path to the "
              "environment variable ENTRYPOINT.")
