FROM python:3.8.3-slim

WORKDIR /usr/src/app

ENV PYTHONPATH ./

# add git to the image
RUN apt-get update \
&& apt-get install -y --no-install-recommends git \
&& apt-get purge -y --auto-remove \
&& rm -rf /var/lib/apt/lists/*

# upgrade pip
RUN pip install --upgrade pip

# install Python modules needed by the Python app
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r ./requirements.txt

# install the git repository
RUN pip install --no-cache-dir -e git+https://gitlab.com/fan2tuning/gym-dodgeball.git#egg=gym_dodgeball

# copy files required for the app to run
COPY entrypoint.py /usr/src/app/
COPY __init__.py /usr/src/app/
COPY main /usr/src/app/main
COPY utils /usr/src/app/utils

# run the application
CMD ["python", "./entrypoint.py"]
