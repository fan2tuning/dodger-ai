def hello(name=None):
    if name is not None:
        print(f"Hello {name}!")
    else:
        print("Hello world!")
