# Dodger-AI

WIP

This is a fun small project to create an AI-driven agent operating in a environment in which it has to learn to dodge some projectiles fired by other agent.

This project is meant to run in a docker container and is written in python. The library OpenAI is used to create the gym where our agents will play.

### TODOs
- virtual frame buffer for docker rendering (xvfb)
- add tests in CI Gitlab
- create basic dodger agent

## How to set up the application

### Prerequisites
Docker needs to be installed. Follow the instructions here to install docker-ce https://docs.docker.com/install/linux/docker-ce/ubuntu/

After, run these commands so you don't need to use sudo to run Docker
```bash
> sudo usermod -aG docker your_name
> newgrp docker
```

### Installation
Clone the repository.

Copy the file .env.example.
```bash
> cd ./dodger-ai
> cp .env.example .env
```

### Start the application
```bash
> docker build -t dodger-image .
> docker run --rm --env-file=.env --name dodger-container dodger-image:latest
```

You can run the code directly in Python command line, but you need a python version >=3.6 and to previously install all requirements package. This should be avoided during development for consistency reasons.
```bash
> export PYTHONPATH=./
> python ./main/whatever-main-script.py
```

### Output
