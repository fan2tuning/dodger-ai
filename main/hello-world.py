from utils.helloworld import hello


if __name__ == '__main__':
    names = ["Chang", "Thomas", "Not world", None, __name__]
    for name in names:
        hello(name)
