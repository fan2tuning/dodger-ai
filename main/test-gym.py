import gym


if __name__ == "__main__":

    env = gym.make("CartPole-v0")
    observation = env.reset()
    for i in range(300):
        print(observation)
        observation, _, done, _ = env.step(env.action_space.sample())
        if done:
            print(f"Finished after {i+1} iterations")
            break
    env.close()
