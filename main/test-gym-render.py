import gym


if __name__ == "__main__":

    env = gym.make("CartPole-v0")
    env.reset()
    for i in range(300):
        env.render()  # render cannot run on docker container atm
        env.step(env.action_space.sample())  # random action
    env.close()
