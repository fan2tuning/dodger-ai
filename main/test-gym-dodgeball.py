import gym
import gym_dodgeball  # noqa: F401

env = gym.make("dodgeball-v0")
obs = env.reset()
print(obs)
total_reward = 0
for i in range(50):
    # env.render()  # render cannot run on docker container atm
    obs, rew, done, _ = env.step(env.action_space.sample())  # random action
    print("Observation:")
    print(obs)
    total_reward += rew
    if done:
        print(f"Simulation ending at {i} iterations.")
        print(f"Cumulative reward: {total_reward}")
        break
env.close()
